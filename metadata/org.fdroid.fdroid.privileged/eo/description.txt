Rimarko: F-Droid postulas ĉefuzantan aliron por instali la privilegian aldonaĵon kiel sisteman privatan aplikaĵon.

Ebligas al la kliento <a href="https://f-droid.org/eo/packages/org.fdroid.fdroid>F‑Droid</a> uzi ĉefuzantan aliron «root» por instali, ĝisdatigi kaj forigi aplikaĵojn. La ununura maniero por fari tion estas iĝi sistema aplikaĵo.

Tiucele ekzistas la privilegia aldonaĵo - kiel memstara eta aplikaĵo ĝi povas esti instalita kiel sistema aplikaĵo kaj komuniki kun la ĉefa aplikaĵo per AIDL IPC.

Jen kelkaj avantaĝoj:

* malpliigita uzo de sistema subdisko

 * sistemaj ĝisdatigo ne forigas F-Droid

 * instalado al sistemo per ĉefuzanto estas pli sekura

Anstataŭ tiu ĉi kompilaĵo la plej parto da uzantoj probable volus instali la
